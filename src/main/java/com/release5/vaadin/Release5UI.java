package com.release5.vaadin;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

/**
 * The topmost component of MGPS UI.
 * 
 * @author martin.letendre@release5.com
 * 
 */
@SuppressWarnings("serial")
public abstract class Release5UI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = Release5UI.class, widgetset = "com.release5.vaadin.Release5WidgetSet")
	public static class Servlet extends VaadinServlet {

	}

}
